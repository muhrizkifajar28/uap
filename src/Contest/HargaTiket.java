package Contest;

interface HargaTiket {
    //Do your magic here...

    //sebuah method yang akan diimplementasi oleh kelas yang mengimplements interface ini yaitu class 'TiketKonser'
    double getHarga();
}