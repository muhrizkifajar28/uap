package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...

    
    String namaTiket;
    double hargaTiket;

    //pengimplementasian method dari interface 'HargaTiket'
    @Override
    public double getHarga() {
        return this.hargaTiket;
    }

    public String getNama() {
        return this.namaTiket;
    }
}