/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    static boolean isBerhasil = false;
    public static void main(String[] args) {
        //Do your magic here...

        //pengulangan ini akan terus berjalan ketika pemesanan belum berhasil
        do {
            try {
                PemesananTiket.run();

                //ketika tidak terjadi kesalahan pada 'PemesananTiket.run()' maka variabel 'isBerhasil' berubah menjadi true
                //dan pengulangn akan berhenti
                isBerhasil = true;
            } catch (InvalidInputException e) {
                //ketika terjadi kesalah pada 'PemesananTiket.run()' maka akan mengeksekusi perintah dibawah
                System.out.println("error :" + e.getMessage());
            } catch (InputMismatchException e) {
                //ketika user menginpukan tipe data yang salah, 
                //pada pemesanan saat pemilihan menu tiket user harus menginputkan numerik saja
                System.out.println("error : Pilihan harus berupa angka numerik");
            }
           
        } while (!isBerhasil);

        //ketika pemesanan telah berhasil dan pengulanan berhenti maka akan mengeluarkan
        //detail pemesanan
        System.out.println("----- Detail Pemesanan -----");
        System.out.println("Nama Pemesan\t\t: "+ PemesananTiket.namaPemesan);
        System.out.println("Kode Boking\t\t: "+ generateKodeBooking());
        System.out.println("Tiket yang dipesan\t: "+ PemesananTiket.tiketDipesan.namaTiket);
        System.out.println("Tanggal Pesanan\t\t: "+ getCurrentDate());
        System.out.println("Total Harga\t\t: "+ PemesananTiket.tiketDipesan.getHarga()+" USD");
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}