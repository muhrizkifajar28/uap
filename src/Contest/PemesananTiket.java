package Contest;

import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...

    //attribut berupa nama dari pemesan tiket
    static String namaPemesan;

    //attribut berupa tiket yang dipesan;
    static TiketKonser tiketDipesan;
    
    //method ini akan men throws 'InvalidInputException' ketika terjadi suatu kesalahan
    public static void run() throws InvalidInputException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Selamat datang di pemesanan tiket Coldplay !");

        System.out.print("Masukkan nama pemesan : ");

        //user akan menginputkan nama pemesan kedalam variabel 'namaPemesan'
        namaPemesan = sc.nextLine();

        //variabel tersebut akan di cek apakah melewati batas yaitu lebih dari sama dengan 10
        //jika iya maka akan mengeluarkan errorMessage
        if (namaPemesan.length() >= 10) {
            throw new InvalidInputException("Nama pemesan harus kurang dari 10 karakter");
        }

        System.out.print("Pilih jenis tiket :");
        System.out.println("\n1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. UNLIMITED EXPERIENCE");
        System.out.print("masukkan pilihan :");

        //user akan menginputkan pilihan kedalam variabel 'pilihan'
        int pilihan = sc.nextInt();

        //variabel tersebut akan dicek apa berada dalam rentang 1-5
        // jika tidak maka akan mengeluarkan errorMessage
        if (pilihan < 1 || pilihan > 5) {
            throw new InvalidInputException("pilihan tiket harus antara 1 hingga 5");
        }

        //pada switch statement ini variabel 'tiketDipesan' akan diisi sesuai pilihan user
        switch (pilihan) {
            case 1:
                tiketDipesan = new CAT8("CAT8", 100);
                break;
            case 2:
                tiketDipesan = new CAT1("CAT1", 150);
                break;
            case 3:
                tiketDipesan = new FESTIVAL("FESTIVAL", 200);
                break;
            case 4:
                tiketDipesan = new VIP("VIP", 300);
                break;
            case 5:
                tiketDipesan = new VVIP("UNLIMITED EXPERIENCE", 400);
                break;
            default:
                break;
        }

        System.out.println("Pemesanan Tiket Berhasil");
    }
}